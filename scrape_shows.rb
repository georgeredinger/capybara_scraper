require 'rubygems'
require 'capybara'
require 'capybara/dsl'
require 'capybara/mechanize'
require 'pry'

Capybara.run_server = false
Capybara.default_driver = :mechanize
Capybara.app_host = 'http://www.usdf.org'

class Region
	include Capybara
	def shows(region_number,year)
		visit("/calendar/competitions.asp?regionpass=#{region_number}&TypePass=All&YearPass=#{year}")
    competition_contact_url = ""
    show_page_url = ""
	  results_url = ""

		table=find_by_id('sub-content')
		rows=table.all('tr')[4..-1]
		rows.each do |row|
      cell=row.all('td')
			cell[0].all('a').each do |url|
        if url.text =~ /Contact\/Official Information/i
          competition_contact_url = url[:href]
				end

				if url.text =~/.*\..*/
          show_page_url = url[:href]
				end

				if url.text == 'Results'
			  	results_url = url[:href] 
			  end
			end

      location_href=cell[1].find('a')[:href]
      show_name=cell.first.find('strong').text
		#	puts "Show_name:#{show_name}"
		#	puts "Location_href:#{location_href}"
		#	puts "Competition_contact_url:#{competition_contact_url}"
		#	puts "Show_page_url:#{show_page_url}"
		#	puts "results_url:#{results_url}"
	  #		puts "USDF#: #{cell[0].text[/USDF# \d+/][/\d+/]}"
		print "."
		end
    rescue 
	    puts "*"*20
	    puts "Region #{region_number}"
	    puts "*"*20
			retry
	end
end

scrape = Region.new
(1..9).each do |region| 
	puts "Scraping Region #{region}"
	scrape.shows(region,2011)
	puts
 end



